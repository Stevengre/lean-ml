import MatchingLogic.Definedness 

namespace ML

namespace Sorted

set_option autoImplicit false 

inductive SORTS where 
| inhabitant 
| Sorts 

class HasSorts (𝕊 : Type) extends HasDefined 𝕊 where 
  inhabitant : 𝕊 
  sorts : 𝕊 

section open HasSorts (inhabitant sorts)

  variable {𝕊 : Type} [HasSorts 𝕊]

  def inhabits (φ : Pattern 𝕊) : Pattern 𝕊 := 
    .symbol HasSorts.inhabitant ⬝ φ
    
  notation (priority := high) "⟦" φ "⟧" => inhabits φ

  def sortedNegation (s : Pattern 𝕊) (φ : Pattern 𝕊) : Pattern 𝕊 := 
    ∼φ ⋀ ⟦s⟧ 

  -- should probably change precedence so that `∈∈` is tighter than logical connectives
  def sortedUniversal (s : Pattern 𝕊) (x : EVar) (φ : Pattern 𝕊) : Pattern 𝕊 := 
    ∀∀ x ((x ∈∈ ⟦s⟧) ⇒ φ) 

  def sortedExistential (s : Pattern 𝕊) (x : EVar) (φ : Pattern 𝕊) : Pattern 𝕊 := 
    ∀∀ x ((x ∈∈ ⟦s⟧) ⋀ φ) 
  
  notation "∀∀ₛ" x "∶" s ";" φ => sortedUniversal s x φ

  notation "∃∃ₛ" x "∶" s ";" φ => sortedExistential s x φ

  def subsort (s₁ s₂ : Pattern 𝕊) : Pattern 𝕊 := 
    ⟦s₁⟧ ⊆⊆ ⟦s₂⟧

  def applicationList (φ : Pattern 𝕊) (ψ : List (Pattern 𝕊)) : Pattern 𝕊 :=
    ψ.foldl Pattern.application φ

  infix:55 "⬝*" => applicationList 

  def universalList (xs : List EVar) (φ : Pattern 𝕊) : Pattern 𝕊 := 
    xs.foldr (init := φ) Pattern.universal

  #reduce universalList [⟨0⟩, ⟨1⟩, ⟨2⟩] (.evar ⟨2⟩ : Pattern Empty) 
#check List.foldr
  -- def sortedUniversalList (ξs : List (EVar × (Pattern 𝕊))) (φ : Pattern 𝕊) : Pattern 𝕊 := 
  --   ξs.foldr (init := φ) <| fun ⟨x, s⟩ ψ => ∀∀ₛ x ∶ s ; ψ

  -- #reduce sortedUniversalList [⟨⟨0⟩, ⟨0⟩ : Pattern 𝕊⟩] (.evar ⟨2⟩ : Pattern Empty) 

  def sortedUniversalList (xs : List EVar) (ss : List (Pattern 𝕊)) (φ : Pattern 𝕊) : Pattern 𝕊 :=  
    xs.zip ss |>.foldr (init := φ) <| fun ⟨x, s⟩ u => ∀∀ₛ x ∶ s ; u

  notation "∀∀ₛ*" => sortedUniversalList 



  def sortName (s : 𝕊) (z : EVar := ⟨0⟩) : Pattern 𝕊 := 
    (.symbol s) ∈∈ ⟦.symbol sorts⟧ ⋀ ∃∃ z (.symbol s ≡ z)

  def nonemptyInhabitant (s : 𝕊) : Pattern 𝕊 := 
    ∼(⟦.symbol s⟧ ≡ ⊥)

  def function (xs : List EVar) (y : EVar) (src : List (Pattern 𝕊)) (tgt : Pattern 𝕊) (f : Pattern 𝕊) : Pattern 𝕊 := 
    ∀∀ₛ* xs src (∃∃ₛ y ∶ tgt ; f ⬝* xs ≡ y) 

  def partialFunction (xs : List EVar) (y : EVar) (src : List (Pattern 𝕊)) (tgt : Pattern 𝕊) (f : Pattern 𝕊) : Pattern 𝕊 := 
    ∀∀ₛ* xs src (∃∃ₛ y ∶ tgt ; f ⬝* xs ⊆⊆ y) 

  def predicate (xs : List EVar) (args : List (Pattern 𝕊)) (π : Pattern 𝕊): Pattern 𝕊 := 
    ∀∀ₛ* xs args <| (π ⬝* xs ≡ ⊤) ⋁ (π ⬝* xs ≡ ⊥)

end 



section 
  
  class ManySorted 
    (𝕊 : Type) [HasSorts 𝕊] 
    (S : Type)
    (F : Type)
    (P : Type)
    (Γ : Premises 𝕊) extends HasDefinedness Γ 
  where 
    SORT_NAME {s : 𝕊} {z : EVar} : sortName s z ∈ Γ 
    NONEMPTY_INHABITANT {s : 𝕊} : nonemptyInhabitant s ∈ Γ
    FUNCTION {xs} {y} {src} {tgt} {f : F} : function xs y src tgt f ∈ Γ
    PREDICATE {xs} {args} {π : P} : predicate xs args π ∈ Γ
    


end 
























