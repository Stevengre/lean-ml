/-
Copyright (c) 2022 Institute for Logic and Data Science (ILDS). All rights reserved.
Released under MIT license as described in the file LICENSE.
Authors: Andrei Burdușa, Horațiu Cheval
-/

import MatchingLogic.Util
import MatchingLogic.Pattern
import MatchingLogic.Proof 
import MatchingLogic.PrettyPrinterOptions
import MatchingLogic.Substitution

namespace ML

set_option autoImplicit false 
set_option Pattern.pp_interpret_hiding false


structure Interpretation (𝕊 : Type) (M : Type) where 
  evalSymb : 𝕊 → Set M 
  evalApp : M → M → Set M 

structure Valuation (M : Type) where 
  evalEvar : EVar → M 
  evalSvar : SVar → Set M 
  
variable {𝕊 : Type} {M : Type}

@[simp]
def Pattern.interpret (I : Interpretation 𝕊 M) (eval : Valuation M) (φ : Pattern 𝕊) : Set M :=
match φ with 
| evar x => { eval.evalEvar x }
| svar i => eval.evalSvar i
| symbol σ => I.evalSymb σ
-- TODO (Andrei B): Use set notation?
| φ ⇒ ψ => fun m => (φ.interpret I eval m) → (ψ.interpret I eval m)
| ∃∃ x φ => fun m => ∃ a : M, φ.interpret I ⟨(Function.update eval.evalEvar x a), eval.evalSvar⟩ m
| μ X φ =>
  -- m ∈ ⋂ {B ⊆ M | e(X ← B)⁺ (φ) ⊆ B}
  fun m : M =>
    -- for all B ⊆ M,
    ∀ B : Set M,
      -- if e(X ← B) ⊆ B holds
      (∀ m,
        φ.interpret I ⟨eval.evalEvar, Function.update eval.evalSvar X B⟩ m → B m
      ) →
      -- then m ∈ B
      B m
| p₁ ⬝ p₂ => fun m => ∃ a : M, ∃ b : M, (p₁.interpret I eval a) ∧ (p₂.interpret I eval b) ∧ I.evalApp a b m
| ⊥ => fun _ => False

def muInterpret (I : Interpretation 𝕊 M) (eval : Valuation M) (φ : Pattern 𝕊) (X : SVar) := 
  fun m : M => ∀ B : Set M, φ.interpret I ⟨eval.evalEvar, Function.update eval.evalSvar X B⟩ m → B m

#check @muInterpret
/-- 
  Notation for `Pattern.interpret` which purposefully hides all the other parameters beyond the pattern
  (like the interpretation, variable valuation, etc.) as in most cases those are fixed and only obscure the view. 
  The notation is never to be typed; it is just for printing purposes. 
  Use `set_option pp_interpret_hiding false` to disable this pretty printing.   
-/
notation "∥" φ "//" e ", " I "∥" => Pattern.interpret I e φ
notation "∥" φ "∥" => _ 
open Lean PrettyPrinter Delaborator SubExpr in 
@[delab app.ML.Pattern.interpret] 
def delabInterpret : Delab := do 
  let e ← getExpr 
  guard $ e.getAppNumArgs == 6 -- careful: `Pattern.interpret` has actually `6` arguments, because `Set M` means `M → Prop`.
  let ppInterpretHiding ← getBoolOption Pattern.pp_interpret_hiding.name
  guard ppInterpretHiding 
  let pattern ← withAppFn $ withAppArg delab --the second last argument 
  `(∥$pattern∥)

variable (φ : Pattern Empty)

def Valuation.satifies (I : Interpretation 𝕊 M) (eval : Valuation M) : Pattern 𝕊 → Prop := 
  fun φ => ∀ m : M, φ.interpret I eval m

notation I "_at_" eval "⊧ᴵ" φ => Valuation.satifies I eval φ

@[simp]
def Interpretation.satisfies (I : Interpretation 𝕊 M) : Pattern 𝕊 → Prop :=
  fun φ => ∀ eval : Valuation M, I _at_ eval ⊧ᴵ φ

infix:(arrow_precedence + 1) " ⊧ᴵ " => Interpretation.satisfies

@[simp]
def Interpretation.satisfiesAll (I : Interpretation 𝕊 M) : Premises 𝕊 → Prop := 
  fun Γ => ∀ γ ∈ Γ, I ⊧ᴵ γ

infix:(arrow_precedence + 1) " ⊧ᴵP " => Interpretation.satisfiesAll

-- todo: better names 

-- TODO (Andrei B): Global semantic consequence?
@[simp]
def entails (Γ : Premises 𝕊) (φ : Pattern 𝕊) : Prop := 
  ∀ {M : Type} [Inhabited M] {I : Interpretation 𝕊 M}, I ⊧ᴵP Γ → I ⊧ᴵ φ

def substEVarValuation { M : Type } (eval : Valuation M) (x : EVar) (a : M) (arg : EVar) := if arg == x then a else eval.evalEvar arg 
  
infix:(arrow_precedence + 1) " ⊧ " => entails 

attribute [simp] or_def_imp_neg in
section 

  variable {Γ : Premises 𝕊} {φ ψ : Pattern 𝕊} {I : Interpretation 𝕊 M} {e : Valuation M}

  set_option Pattern.pp_interpret_hiding true  

  @[simp] 
  theorem interpret_bottom : ∀ {m : M}, ⊥.interpret I e m ↔ False := by simp

  
  @[simp]
  theorem interpret_neg : ∀ {m : M}, (∼φ).interpret I e m ↔ ¬φ.interpret I e m := by 
    intros m ; apply Iff.intro <;> (intros h ; simpa)

  @[simp]
  theorem interpret_impl : ∀ {m : M}, (φ ⇒ ψ).interpret I e m ↔ (φ.interpret I e m → ψ.interpret I e m) := by simp

  @[simp]
  theorem interpret_app : ∀ {m : M}, (φ ⬝ ψ).interpret I e m ↔ ∃ a : M, ∃ b : M, (φ.interpret I e a) ∧ (ψ.interpret I e b) ∧ I.evalApp a b m := by simp 

  @[simp]
  theorem interpret_disj : ∀ {m : M}, (φ ⋁ ψ).interpret I e m ↔ φ.interpret I e m ∨ ψ.interpret I e m := by 
    intros m
    apply Iff.intro <;> intros h 
    . simpa 
    . rw [or_def_imp_neg] at h
      exact h

  @[simp]
  theorem interpret_exists {x : EVar} : ∀ {m : M}, (∃∃ x φ).interpret I e m ↔ ∃ a : M, φ.interpret I ⟨(Function.update e.evalEvar x a), e.evalSvar⟩ m := by simp 

  @[simp] 
  theorem implies_false_is_neg { p : Prop } : (p → False) ↔ ¬p := by 
    apply Iff.intro <;> intro h <;> intro hp <;> exact h hp 

  @[simp]
  theorem equivalence { p q : Prop } : (p → q) ∧ (q → p) ↔ (p ↔ q) := by 
    apply Iff.intro <;> intros h 
    . cases h with | intro hLeft hRight =>
      apply Iff.intro <;> intros hLocal 
      . exact hLeft (hRight (hLeft (hRight (hLeft hLocal))))
      . exact hRight (hLeft (hRight (hLeft (hRight hLocal))))
    . cases h with | intro hLeft hRight => 
      exact And.intro hLeft hRight 

  @[simp] 
  theorem interpret_equiv: ∀ {m : M}, (φ ⇔ ψ).interpret I e m ↔ (φ.interpret I e m ↔ ψ.interpret I e m) := by 
    intros m
    apply Iff.intro <;> intros h 
    . simp_all
    . simp_all  

  @[simp]
  theorem interpret_conj : ∀ {m : M}, (φ ⋀ ψ).interpret I e m ↔ φ.interpret I e m ∧ ψ.interpret I e m := by 
    intro m 
    apply Iff.intro <;> intros h 
    . simp at h
      assumption 
    . simp at * 
      assumption        

  -- Similar to Proposition 3.7 (vii)
  theorem v_sat_conj:
    Valuation.satifies I e (φ ⋀ ψ) ↔
      (Valuation.satifies I e φ ∧ Valuation.satifies I e ψ)
  := by
    simp only [Valuation.satifies, interpret_conj]
    constructor
    . intro
      constructor <;> (intro; simp only [*])
    . intro ⟨_, _⟩ _
      simp only [*]

  -- Similar to Proposition 3.7 (xi)
  theorem v_sat_equiv:
    Valuation.satifies I e (φ ⇔ ψ) ↔
      ( (Valuation.satifies I e (φ ⇒ ψ)) ∧
        (Valuation.satifies I e (ψ ⇒ φ ))
      )
  := by 
    simp only [Pattern.equivalence]
    apply v_sat_conj 

  theorem weakening_disj_sound : Γ ⊧ φ ⇒ φ ⋁ ψ := by 
    intros _ _ _ _ _ _ _
    simp [*]

  theorem weakening_conj_sound : Γ ⊧ φ ⋀ ψ ⇒ φ := by 
    intros _ _ _ _ _ _ h 
    rw [interpret_conj] at h
    cases h <;> assumption 

  theorem contraction_disj_sound : Γ ⊧ φ ⋁ φ ⇒ φ := by 
    intros _ _ _ _ _ _ h 
    rw [interpret_disj] at h 
    cases h <;> assumption 

  theorem contraction_conj_sound : Γ ⊧ φ ⇒ φ ⋀ φ := by 
    intros _ _ _ _ _ _ h 
    rw [interpret_conj] at *
    simp at * 
    assumption 
  
  theorem permutation_disj_sound : Γ ⊧ φ ⋁ ψ ⇒ ψ ⋁ φ := by 
    intros _ _ _ _ _ _ h 
    rw [interpret_disj]  
    rwa [interpret_disj, Or.comm] at h

  theorem permutation_conj_sound : Γ ⊧ φ ⋀ ψ ⇒ ψ ⋀ φ := by 
    intros _ _ _ _ _ _ h 
    rw [interpret_conj] at h 
    rw [And.comm] at h
    rw [interpret_conj] at *
    assumption 

  theorem exFalso_sound : Γ ⊧ ⊥ ⇒ φ := by 
    intros _ _ _ _ _ _ h 
    rw [interpret_bottom] at h
    exact False.elim h 

  theorem excluddedMiddle_sound : Γ ⊧ φ ⋁ ∼φ := by 
    intros _ _ _ _ _ _ h 
    rw [interpret_neg] at h 
    assumption 
    
  theorem modus_ponens_sound : Γ ⊧ φ → Γ ⊧ φ ⇒ ψ → Γ ⊧ ψ := by 
    intros h₁ h₂ M _ I hΓ e m 
    specialize h₁ hΓ e m 
    specialize h₂ hΓ e m 
    rw [interpret_impl] at h₂
    exact h₂ h₁

  theorem syllogism_sound { χ : Pattern 𝕊 } : Γ ⊧ φ ⇒ ψ → Γ ⊧ ψ ⇒ χ → Γ ⊧ φ ⇒ χ := by 
    intros h₁ h₂ M _ I hΓ e m 
    specialize h₁ hΓ e m 
    specialize h₂ hΓ e m 
    rw [interpret_impl] at h₁ 
    rw [interpret_impl] at h₂ 
    rw [interpret_impl] at * 
    exact λ x => h₂ $ h₁ x

  theorem importation_sound { χ : Pattern 𝕊 } : Γ ⊧ φ ⇒ ψ ⇒ χ → Γ ⊧ (φ ⋀ ψ) ⇒ χ := by 
    intros h₀ _ _ _ I eval h 
    rw [interpret_impl] 
    rw [interpret_conj]
    exact λ x => And.elim (h₀ I eval h) x

  theorem exportation_sound { χ : Pattern 𝕊 } : Γ ⊧ (φ ⋀ ψ) ⇒ χ → Γ ⊧ φ ⇒ ψ ⇒ χ := by 
    intros h₀ _ _ I hΓ eval h 
    specialize h₀ hΓ eval h 
    rw [interpret_impl, interpret_conj] at h₀
    rw [interpret_impl, interpret_impl] at *
    intros hφ hψ
    exact h₀ $ And.intro hφ hψ

  theorem expansion_sound { χ : Pattern 𝕊 } : Γ ⊧ φ ⇒ ψ → Γ ⊧ χ ⋁ φ ⇒ χ ⋁ ψ := by 
    intros h₀ _ _ I hΓ eval h 
    specialize h₀ hΓ eval h 
    rw [interpret_impl] at h₀
    rw [interpret_impl, interpret_disj, interpret_disj] at *
    intro hφorψ
    apply Or.elim hφorψ 
    . exact Or.inl  
    . intro hφ 
      exact Or.inr $ h₀ hφ

  open Classical

  syntax "simp_fv_decide_or" : tactic
  macro_rules
  | `(tactic| simp_fv_decide_or) =>
     `( tactic|
          simp only
            [ *
            , Bool.decide_iff, Pattern.isFreeEvar, Pattern.isFreeSvar
            , Or.inl, Or.inr
            ]
      )

  -- Similar to Proposition 3.8
  set_option Pattern.pp_interpret_hiding false in 
  theorem only_free_var_relevant : ∀ (fe₁ fe₂ : EVar → M) (fs₁ fs₂ : SVar → Set M),
    (∀ x, φ.isFreeEvar x → fe₁ x = fe₂ x) →
    (∀ X, φ.isFreeSvar X → fs₁ X = fs₂ X) →
    ∀ m,
      φ.interpret I ⟨ fe₁, fs₁ ⟩ m ↔
      φ.interpret I ⟨ fe₂, fs₂ ⟩ m
  := by
    induction φ with
    | evar ev =>
      intros _ _ _ _ H₁ _ _
      simp only [Pattern.interpret]
      rw [H₁ ev]
      simp only [Pattern.isFreeEvar]
    | svar sv =>
      intros _ _ _ _ _ H₂ _
      simp only [Pattern.interpret]
      rw [H₂ sv]
      simp only [Pattern.isFreeSvar]
    | bottom =>
      intros
      simp only [*, Pattern.interpret]
    | symbol =>
      intros
      simp only [*, Pattern.interpret]
    | application ψ χ ih₁ ih₂ =>
      intros fe₁ fe₂ fs₁ fs₂ H₁ H₂ m
      apply Iff.intro
      . intro ⟨a₁, a₂, h₁, h₂, h₃⟩
        exists a₁; exists a₂
        apply And.intro <;> try apply And.intro <;> try assumption
        . rwa [ih₁ fe₂ fe₁ fs₂ fs₁] <;> intros <;> simp_fv_decide_or
        . rwa [ih₂ fe₂ fe₁ fs₂ fs₁] <;> intros <;> simp_fv_decide_or
      . intro ⟨a₁, a₂, h₁, h₂, h₃⟩
        exists a₁; exists a₂
        apply And.intro <;> try apply And.intro <;> try assumption
        . rwa [ih₁ fe₁ fe₂ fs₁ fs₂] <;> intros <;> simp_fv_decide_or
        . rwa [ih₂ fe₁ fe₂ fs₁ fs₂] <;> intros <;> simp_fv_decide_or
    | implication ψ χ ih₁ ih₂ =>
      intros fe₁ fe₂ fs₁ fs₂ H₁ H₂ m
      simp only [Pattern.interpret]
      rw [ih₁, ih₂] <;> intros <;> simp_fv_decide_or
    | existential a ψ ih =>
      intros fe₁ fe₂ fs₁ fs₂ H₁ H₂ m
      simp only [Pattern.isFreeEvar] at H₁
      simp only [Pattern.interpret]
      apply Iff.intro <;> (intro ⟨a₁, _⟩; exists a₁; rw [ih] <;> try assumption)
      . intro x _
        simp only [Function.update]
        apply @Classical.byCases (x = a)
          <;> intro <;> simp only [*, Pattern.isFreeEvar, dif_pos]
      . intro x _
        rw [H₂]
        simp only [Pattern.isFreeSvar, *]
      . intro x _
        simp only [Function.update]
        apply @Classical.byCases (x = a)
          <;> intro <;> simp only [*, Pattern.isFreeEvar, dif_pos]
    | mu X ψ ih =>
      intros fe₁ fe₂ fs₁ fs₂ H₁ H₂ m
      simp only [Pattern.interpret]
      apply Iff.intro <;>
        ( intro H B Hψ
          apply H
          intro m₀
          specialize Hψ m₀
          try rw
            [ih fe₁ fe₂ (Function.update fs₁ X B) (Function.update fs₂ X B)]
            at *
          repeat assumption
          . intros X' Hf
            simp only [Function.update]
            apply @Classical.byCases (X' = X)
              <;> intros <;> simp only [*, Pattern.isFreeSvar, dif_pos]
        )

  set_option Pattern.pp_interpret_hiding false in 
  @[simp]
  -- Similar to Proposition 3.70, for δ := y
  theorem subf_xy_eq_eval_xey {x y : EVar}  : ∀ (m : M) (e : Valuation M),
    Pattern.substitutableForEvarIn x y φ →  
    Pattern.interpret I e (Pattern.substEvar φ x y) m
      =
      φ.interpret I ⟨ Function.update e.evalEvar x (e.evalEvar y), e.evalSvar ⟩ m
  := by
    apply @Classical.byCases (φ.isFreeEvar x)
    . intro Hfv
      induction φ with
      | evar ev =>
        intros
        rw [Pattern.isFreeEvar, Ne.ite_eq_left_iff] at Hfv
          <;> try intro; contradiction
        simp only
          [ Pattern.substEvar, Pattern.interpret, Hfv, Function.update
          , ite_true, eq_rec_constant, dite_eq_ite
          ]
      | svar sv => intros; simp only [Pattern.interpret]
      | bottom => intros; simp only [Pattern.interpret]
      | symbol => intros; simp only [Pattern.interpret]
      | application ψ χ ih₁ ih₂ =>
        simp only [Pattern.interpret, Pattern.substitutableForEvarIn]
        intro m e ⟨Hsφ, Hsχ⟩
        simp only [eq_iff_iff]
        apply Iff.intro
        . intro ⟨a₁, a₂, h₁, h₂, h₃⟩
          exists a₁; exists a₂
          apply And.intro <;> try apply And.intro <;> try assumption
          . apply @Classical.byCases (ψ.isFreeEvar x)
            . intro
              rw [← ih₁] <;> try assumption
            . intro
              rw [← case_not_fv ψ] <;> try assumption
          . apply @Classical.byCases (χ.isFreeEvar x)
            . intro
              rw [← ih₂] <;> try assumption
            . intro
              rw [← case_not_fv χ] <;> try assumption
        . intro ⟨a₁, a₂, h₁, h₂, h₃⟩
          exists a₁; exists a₂
          apply And.intro <;> try apply And.intro <;> try assumption
          . apply @Classical.byCases (ψ.isFreeEvar x)
            . intro
              rw [ih₁] <;> try assumption
            . intro
              rw [case_not_fv ψ] <;> try assumption
          . apply @Classical.byCases (χ.isFreeEvar x)
            . intro
              rw [ih₂] <;> try assumption
            . intro
              rw [case_not_fv χ] <;> try assumption
      | implication ψ χ ih₁ ih₂ =>
        simp only [Pattern.interpret, Pattern.substitutableForEvarIn]
        intro m e ⟨Hsφ, Hsχ⟩
        apply @Classical.byCases (ψ.isFreeEvar x)
        . intro
          rw [ih₁] <;> try assumption
          apply @Classical.byCases (χ.isFreeEvar x)
          . intro
            rw [ih₂] <;> try assumption
          . intro
            rw [case_not_fv χ] <;> try assumption
        . intro
          rw [case_not_fv ψ] <;> try assumption
          apply @Classical.byCases (χ.isFreeEvar x)
          . intro
            rw [ih₂] <;> try assumption
          . intro
            rw [case_not_fv χ] <;> try assumption
      | existential z ψ ih =>
        simp only
          [ Pattern.substitutableForEvarIn, Pattern.isFreeEvar
          , Bool.ite_eq_true_distrib
          , if_false_left_eq_and, if_true_right_eq_or, not_not, or_def_imp_neg
          ]
        simp only
          [ Pattern.isFreeEvar, Bool.ite_eq_true_distrib, if_false_left_eq_and
          ] at Hfv
        cases Hfv with | intro Hneq_xz Hfv =>
        intro m e H
        cases H ⟨Hneq_xz, Hfv⟩ with | intro Hneq_zy _ =>
        simp only [Pattern.substEvar, Pattern.interpret, Hneq_xz, eq_iff_iff]
        have neq_y_z : ¬y = z := by intro Heq; simp only [Heq] at *;
        have neq_z_y : ¬z = x := by intro Heq; simp only [Heq] at *;
        apply Iff.intro
        . intro ⟨a, hₐ⟩
          exists a
          rw [ih] at hₐ <;> try assumption
          simp only
            [Function.update, neq_y_z, eq_rec_constant, dite_eq_ite, ite_false]
            at hₐ
          rw [Function.update_comm Hneq_xz]
          simp only [Function.update, hₐ, eq_rec_constant, dite_eq_ite]
        . intro ⟨a, hₐ⟩
          exists a
          rw [ih] <;> try assumption
          rw [Function.update_comm neq_z_y]
          simp only [Function.update, eq_rec_constant, dite_eq_ite] at hₐ
          simp only
            [ hₐ
            , Function.update, eq_rec_constant, neq_y_z, dite_eq_ite, ite_false
            ]
      | mu sv ψ ih =>
        intro m e Hs
        simp only [Pattern.interpret, eq_iff_iff]
        simp only [Pattern.isFreeEvar] at Hfv
        simp only [Pattern.substitutableForSvarIn] at Hs
        apply Iff.intro <;>
        ( intro H₁ B H₂
          apply H₁
          intro m
          try simp only [ih Hfv _ _ Hs] at *
          apply H₂
        )
    . intros Hnot_fv
      apply case_not_fv φ Hnot_fv
  where
    case_not_fv {x y : EVar} φ (Hnot_fv : ¬Pattern.isFreeEvar φ x = true) m e :
      Pattern.substitutableForEvarIn x y φ →  
      Pattern.interpret I e (Pattern.substEvar φ x y) m
        =
        φ.interpret I ⟨Function.update e.evalEvar x (e.evalEvar y), e.evalSvar⟩ m
    := by
      intros
      rw [Pattern.subst_not_free_evar _ _ _ Hnot_fv]
      rw [only_free_var_relevant]
      . intros x₁ H₁
        simp only [Function.update]
        apply @Classical.byCases (x₁ = x)
        . intros eq
          simp only [eq] at H₁; contradiction
        . intros neq
          simp only [neq, dif_neg]
      . intros
        simp only [Function.update]

  set_option Pattern.pp_interpret_hiding false in 
  theorem existQuan_sound {x y : EVar} :
    Pattern.substitutableForEvarIn x y φ → Γ ⊧ φ[x ⇐ᵉ Pattern.evar y] ⇒ ∃∃ x φ
  := by
      intros h₁ M _ I _ eval m hₘ
      simp only [Pattern.interpret]
      exists eval.evalEvar y
      -- Corollary 3.71
      rw [subf_xy_eq_eval_xey _ _ h₁] at hₘ
      apply hₘ

  theorem propagationBottomLeft_sound {c : Pattern 𝕊} : Γ ⊧ c ⬝ ⊥ ⇒ ⊥ := by
    intros _ _ _ _ _ _ h
    match h with
    | ⟨_, _, _, _, _⟩ =>
    assumption 

  theorem propagationBottomRight_sound {c : Pattern 𝕊} : Γ ⊧ ⊥ ⬝ c ⇒ ⊥ := by
    intros _ _ _ _ _ _ h
    match h with
    | ⟨_, _, _, _, _⟩ =>
    assumption 

  set_option Pattern.pp_interpret_hiding false in 
  theorem framing_right_sound {χ} : Γ ⊧ φ ⇒ ψ → Γ ⊧ χ ⬝ φ ⇒ χ ⬝ ψ := by
    intros H
    intros M _ I hΓ e m hφ
    dsimp at hφ ⊢
    match hφ with
    | ⟨a, b, ha, hb, happ⟩ =>
    exists a; exists b
    apply And.intro
    . exact ha
    . dsimp at H 
      specialize H hΓ e b
      rw [interpret_impl] at H
      exact ⟨H hb, happ⟩
  
  set_option Pattern.pp_interpret_hiding false in 
  theorem framing_left_sound {χ} : Γ ⊧ φ ⇒ ψ → Γ ⊧ φ ⬝ χ ⇒ ψ ⬝ χ := by
    intros h 
    intros M _ I hΓ e m hφ
    dsimp at hφ ⊢ 
    match hφ with
    | ⟨a, b, ha, hb, happ⟩ =>
    exists a; exists b
    apply And.intro 
    . specialize h hΓ e a 
      dsimp at h 
      exact h ha 
    . apply And.intro <;> assumption

  theorem implies_false_implies_is_disj { p q : Prop } : ((p → False) → q) ↔ p ∨ q := by 
    apply Iff.intro <;> intro h 
    . rw [implies_false_is_neg] at h
      simpa 
    . rw [implies_false_is_neg] at * 
      simp [*] at * 
      assumption 

  set_option Pattern.pp_interpret_hiding false in 
  theorem propagationDisjLeft_sound {c : Pattern 𝕊} :
    Γ ⊧ c ⬝ (φ ⋁ ψ) ⇒ (c ⬝ φ) ⋁ (c ⬝ ψ)
  := by
      intros _ _ _ _ _ _ h 
      dsimp at * 
      simp_rw [implies_false_implies_is_disj] at * 
      match h with
      | ⟨a, b, h₁, h₂, h₃⟩ =>
      apply Or.elim h₂
      . intro
        apply Or.inl
        exists a; exists b
      . intro
        apply Or.inr
        exists a; exists b
      
  set_option Pattern.pp_interpret_hiding false in 
  theorem propagationDisjRight_sound {c : Pattern 𝕊}
    : Γ ⊧ (φ ⋁ ψ) ⬝ c ⇒ (φ ⬝ c) ⋁ (ψ ⬝ c)
  := by
    intros _ _ _ _ _ _ h
    dsimp at * 
    simp_rw [implies_false_implies_is_disj] at *
    match h with
    | ⟨a, b, h₁, h₂, h₃⟩ =>
    apply Or.elim h₁
    . intro
      apply Or.inl
      exists a; exists b
    . intro 
      apply Or.inr
      exists a; exists b

  instance : Morphism (Pattern.interpret I e) where
    morphism_false := fun _ => interpret_bottom
    morphism_impl := fun _ _ _ => interpret_impl 

  theorem tautology_sound : Tautology φ → Γ ⊧ φ := by 
    intro htaut
    intros _ _ _ _ _ m 
    exact htaut _ m
    
  theorem premise_sound : φ ∈ Γ → Γ ⊧ φ := by 
    intro h 
    intros _ _ _ morphism m 
    exact morphism φ h m
  
  theorem existence_sound {x : EVar} : Γ ⊧ ∃∃ x (Pattern.evar x) := by
    intro _ _ _ _ _ h 
    simp only [Pattern.interpret, Function.update_same]
    use h
    triv

  set_option Pattern.pp_interpret_hiding false in
  theorem existGen_sound {x : EVar} (Hnfv : ¬ψ.isFreeEvar x) :
    Γ ⊧ φ ⇒ ψ → Γ ⊧ ∃∃ x φ ⇒ ψ
  := by
    intros h M _ I hΓ e m
    simp only [Pattern.interpret] at *
    intro ⟨a, hφ⟩ 
    let e' : Valuation M := ⟨Function.update e.evalEvar x a, e.evalSvar⟩
    specialize h hΓ e' m
    have hψ := h hφ
    rw
      [ only_free_var_relevant
          (Function.update e.evalEvar x a)
          e.evalEvar
          e.evalSvar
          e.evalSvar
      ] at hψ
    assumption
    { intros x₀ Hfv
      simp only [Function.update]
      apply @Classical.byCases (x₀ = x) <;> (intro; simp only [dif_neg, *] at *)
    }
    { intros; rfl }

  set_option Pattern.pp_interpret_hiding false in 
  theorem union_of_app_left {m m₁ m₂ : M} {x : EVar} : ((φ.interpret I e m₁) ∧ (∃ a, ψ.interpret I { evalEvar := Function.update e.evalEvar x a, evalSvar := e.evalSvar } m₂) ∧ (Interpretation.evalApp I m₁ m₂ m)) ↔ 
     (∃ a, (φ.interpret I e m₁) ∧ (ψ.interpret I { evalEvar := Function.update e.evalEvar x a, evalSvar := e.evalSvar } m₂) ∧ Interpretation.evalApp I m₁ m₂ m) := by 
      apply Iff.intro <;> intros h 
      . simp [*] at *  
      . simp [*] at * 
        exact h 

  set_option Pattern.pp_interpret_hiding false in 
  theorem union_of_app_right {m m₁ m₂ : M} {x : EVar} : ((∃ a, φ.interpret I { evalEvar := Function.update e.evalEvar x a, evalSvar := e.evalSvar } m₁) ∧ (ψ.interpret I e m₂) ∧ (Interpretation.evalApp I m₁ m₂ m)) ↔ 
     (∃ a, (φ.interpret I { evalEvar := Function.update e.evalEvar x a, evalSvar := e.evalSvar } m₁) ∧ (ψ.interpret I e m₂) ∧ Interpretation.evalApp I m₁ m₂ m) := by 
      apply Iff.intro <;> intros h 
      . simp [*] at *  
      . simp [*] at * 
        exact h 

    
  set_option Pattern.pp_interpret_hiding false in 
  theorem propagationExistLeft_sound
    {ψ : Pattern 𝕊} {x : EVar} (Hnfv : ¬ψ.isFreeEvar x) :
      Γ ⊧ ψ ⬝ ∃∃ x φ ⇒ ∃∃ x (ψ ⬝ φ)
  := by
    intros M _ I _ e m
    simp only [Pattern.interpret]
    intro ⟨a₁, a₂, Hψ, ⟨a₃, Hφ⟩, He⟩ 
    exists a₃; exists a₁; exists a₂
    simp only [*, and_true]
    rw
      [ only_free_var_relevant
          (Function.update e.evalEvar x a₃)
          e.evalEvar
          e.evalSvar
          e.evalSvar
      ]; assumption
    { intros x₀ Hfv
      simp only
        [Function.update, eq_rec_constant, dite_eq_ite, ite_eq_right_iff]
      intros; simp only [*] at *
    }
    { simp only [implies_true] }

  set_option Pattern.pp_interpret_hiding false in 
  theorem propagationExistRight_sound
    {ψ : Pattern 𝕊} {x : EVar} (Hnfv : ¬ψ.isFreeEvar x) :
      Γ ⊧ ∃∃ x φ ⬝ ψ ⇒ ∃∃ x (φ ⬝ ψ)
  := by
    intros M _ I _ e m 
    simp only [Pattern.interpret]
    intro ⟨a₁, a₂, ⟨a₃, Hφ⟩, ⟨Hψ, He⟩⟩
    exists a₃; exists a₁; exists a₂
    simp only [*, and_true, true_and]
    rw
      [ only_free_var_relevant
          (Function.update e.evalEvar x a₃)
          e.evalEvar
          e.evalSvar
          e.evalSvar
      ]; assumption
    { intros x₀ Hfv
      simp only
        [Function.update, eq_rec_constant, dite_eq_ite, ite_eq_right_iff]
      intros; simp only [*] at *
    }
    { simp only [implies_true] }

  theorem dni { p : Prop } : p → ¬¬p := 
    fun hp : p =>
    fun hnp : ¬p => 
    absurd hp hnp 

  theorem dne { p : Prop } : ¬¬p → p := 
    fun hnnp : ¬¬p =>
    Or.elim (Classical.em p) 
    ( fun hp : p => show p from hp )
    ( fun hnp : ¬p =>
      have hf : False := absurd hnp hnnp 
      show p from False.elim hf 
    )
    
  @[simp]
  theorem dn { p : Prop } : ¬¬p ↔ p := by 
    apply Iff.intro <;> intros h 
    . exact dne h 
    . exact dni h

  @[simp]
  theorem neg_of_impl_left { p q : Prop } : ¬(p → q) → (p ∧ ¬q) := by 
    intro h 
    simp [*] at h 
    exact h  

  @[simp]
  theorem neg_of_impl_right { p q : Prop } : (p ∧ ¬q) → ¬(p → q) := by 
    intro h
    simp [*] 
 
  @[simp]
  theorem neg_of_impl { p q : Prop } : ¬(p → q) ↔ p ∧ ¬q := by
    apply Iff.intro <;> intros h 
    . exact neg_of_impl_left h 
    . exact neg_of_impl_right h 

  set_option Pattern.pp_interpret_hiding false in
  -- Similar to Proposition 3.95
  theorem context_impl {C : AppContext 𝕊} :
    Valuation.satifies I e (φ ⇒ ψ) → Valuation.satifies I e (C[φ] ⇒ C[ψ])
  := by
    simp only [Valuation.satifies]
    intro H
    induction C with
    | empty => simpa only [AppContext.insert]
    | left χ C' ih =>
      simp only [AppContext.insert, Pattern.interpret] at *
      dsimp at *
      intro m ⟨a₁, a₂, h₁, h₂, h₃⟩
      exists a₁; exists a₂
      simp only [*]
    | right χ C' ih =>
      simp only [AppContext.insert, Pattern.interpret] at *
      intro m ⟨a₁, a₂, h₁, h₂, h₃⟩
      exists a₁; exists a₂
      simp only [*]

  set_option Pattern.pp_interpret_hiding false in  
  -- Similar to Proposition 3.95
  theorem context_equiv {C : AppContext 𝕊} :
    Valuation.satifies I e (φ ⇔ ψ) → Valuation.satifies I e (C[φ] ⇔ C[ψ])
  := by
    simp only [v_sat_equiv]
    intro ⟨H₁, H₂⟩
    constructor <;> (apply context_impl; assumption)

  set_option Pattern.pp_interpret_hiding false in
  -- Similar to Proposition 3.94 (96)
  theorem context_bottom {C : AppContext 𝕊} : ∀ m,
    Pattern.interpret I e (C[⊥]) m ↔ Pattern.interpret I e ⊥ m
  := by
    induction C with
    | empty => intro m; simp only [AppContext.insert]
    | left χ C' ih =>
      intro m
      simp only [AppContext.insert, Pattern.interpret]
      constructor
      . intro ⟨_, _, _, contra, _⟩
        simp only [ih, Pattern.interpret] at contra
      . exfalso
    | right χ C' ih =>
      intro m
      simp only [AppContext.insert, Pattern.interpret]
      constructor
      . intro ⟨_, _, contra, _, _⟩
        simp only [ih, Pattern.interpret] at contra
      . exfalso

  set_option Pattern.pp_interpret_hiding false in  
  -- Similar to Lemma 3.99
  lemma context_empty {C : AppContext 𝕊} :
    (∀ m, Pattern.interpret I e    φ   m ↔ False) →
    (∀ m, Pattern.interpret I e (C[φ]) m ↔ False)
  := by
    intro H
    have H₁ : Valuation.satifies I e (φ ⇔ ⊥) := by
      simp only [Valuation.satifies, interpret_equiv]
      intro m
      rw [H]; rfl
    have H₂ : Valuation.satifies I e (C[φ] ⇔ C[⊥]) := context_equiv H₁
    intro m
    simp only [Valuation.satifies, interpret_equiv] at H₂
    simp only [H₂, context_bottom, Pattern.interpret]

  set_option Pattern.pp_interpret_hiding false in
  -- Similar to Proposition 3.100
  theorem singleton_sound {C₁ C₂ : AppContext 𝕊} {x : EVar} :
    Γ ⊧ ∼(C₁[Pattern.evar x ⋀ φ] ⋀ C₂[Pattern.evar x ⋀ ∼φ])
  := by
    intro M _ I _ e m h
    rw [interpret_conj] at h
    simp only [Pattern.interpret]
    apply @Classical.byCases ((Pattern.interpret I e φ) (e.evalEvar x))
    . intro he
      have
        H_empty : ∀ m, Pattern.interpret I e (Pattern.evar x ⋀ ∼φ) m ↔ False
      := by
        intro m'
        rw [interpret_conj]
        simp only [Pattern.interpret]
        apply @Classical.byCases (m' = (Valuation.evalEvar e x))
        . intro Heq; simp only [Heq, he, and_false]
        . intro Hneq; constructor
          . intro ⟨_, _⟩; apply Hneq; assumption
          . exfalso
      rw [@context_empty _ _ _ _ _ C₂] at h <;> try assumption
      simp only [h]
    . intro he
      have
        H_empty : ∀ m, Pattern.interpret I e (Pattern.evar x ⋀ φ) m ↔ False
      := by
        intro m'
        rw [interpret_conj]
        simp only [Pattern.interpret]
        apply @Classical.byCases (m' = (Valuation.evalEvar e x))
        . intro Heq; simp only [Heq, he, and_false]
        . intro Hneq; constructor
          . intro ⟨_, _⟩; apply Hneq; assumption
          . exfalso
      rw [@context_empty _ _ _ _ _ C₁] at h <;> try assumption
      simp only [h]

  def 𝓕
    (I : Interpretation 𝕊 M) (e : Valuation M) (φ : Pattern 𝕊) (X : SVar)
    (B : Set M) :
    Set M
  :=
    φ.interpret I ⟨ e.evalEvar, Function.update e.evalSvar X B ⟩

  def 𝓖
    (I : Interpretation 𝕊 M) (e : Valuation M) (φ : Pattern 𝕊) (X : SVar)
    (B : Set M) :
    Set M
  :=
    λ a => ¬ (φ.interpret I ⟨ e.evalEvar, Function.update e.evalSvar X B ⟩) a

  def monotone (F : Set M → Set M) :=
    ∀ (B C : Set M), (∀ m, B m → C m) → ∀ m, (F B m) → (F C m)

  set_option Pattern.pp_interpret_hiding false in 
  -- Similar to Proposition 3.79
  theorem 𝓕𝓖_monotone :
    ∀ (φ : Pattern 𝕊) (X : SVar),
      ((¬Pattern.hasFreeNegativeOcc φ X = true) ->
        (∀ (I : Interpretation 𝕊 M) e, monotone (𝓕 I e φ X))
      )
      ∧
      ((¬Pattern.hasFreePositiveOcc φ X = true) ->
        (∀ (I : Interpretation 𝕊 M) e, monotone (𝓖 I e φ X))
      )
  := by
    intros φ X
    induction φ with
    | evar _ => constructor <;> (intros; rw [monotone]; intros; assumption)
    | svar sv =>
      constructor
      . intros _ I e
        rw [monotone]
        intros B C Hmem m
        simp only [𝓕, Function.update]
        apply @Classical.byCases (sv = X)
        . intros Heq
          rw [Heq, Pattern.interpret, Pattern.interpret]
          simp only [Eq.symm, dif_pos]
          exact Hmem m
        . intro Hneq
          simp only [Pattern.interpret, Eq.symm]
          rw [dif_neg, dif_neg] <;> try assumption
          apply id
      . intros Hpos
        apply @Classical.byCases (sv = X)
        . intro Heq
          rw [Pattern.hasFreePositiveOcc, Heq, if_pos] at Hpos; contradiction; rfl
        . intro Heq I e
          rw [monotone]
          intros B C _ m H
          simp only [𝓖, Function.update, Pattern.interpret, Eq.symm, dif_neg]
          rw [dif_neg] <;> try assumption
          simp only [𝓖, Function.update, Pattern.interpret, Eq.symm] at H
          rw [dif_neg] at H <;> try assumption
    | bottom => constructor <;> (intros; rw [monotone]; intros; assumption)
    | symbol => constructor <;> (intros; rw [monotone]; intros; assumption)
    | application ψ χ ih₁ ih₂ =>
      cases ih₁ with | intro ih₁𝓕 ih₁𝓖 =>
      cases ih₂ with | intro ih₂𝓕 ih₂𝓖 =>
      constructor
      . intros Hpos I e
        rw [monotone] at *
        intros B C hm₁  m HB
        rw [𝓕, Pattern.interpret, ← 𝓕, ← 𝓕] at HB ⊢

        have Hpos := eq_false_of_ne_true Hpos
        simp only
          [Pattern.hasFreeNegativeOcc, Bool.decide_false_iff, not_or] at Hpos
        cases Hpos with | intro Hposψ Hposχ =>

        match HB with
        | ⟨a, b, H₁, H₂, H₃⟩ =>
        exists a; exists b
        constructor <;> try constructor <;> try assumption
        . apply ih₁𝓕 <;> assumption
        . apply ih₂𝓕 <;> assumption
      . intros Hpos I e
        rw [monotone] at *
        intros B C hm₁ m HB
        rw [𝓖, Pattern.interpret, ← 𝓕, ← 𝓕] at HB ⊢

        have Hpos := eq_false_of_ne_true Hpos
        simp only
          [Pattern.hasFreePositiveOcc, Bool.decide_false_iff, not_or] at Hpos
        cases Hpos with | intro Hposψ Hposχ =>

        intro ⟨a, b, H₁, H₂, H₃⟩
        apply HB
        exists a; exists b
        constructor <;> try constructor <;> try assumption
        . specialize ih₁𝓖 Hposψ I e B C hm₁ a
          rw [𝓖, 𝓖, not_imp_not, ← 𝓕, ← 𝓕] at ih₁𝓖
          simp only [*]
        . specialize ih₂𝓖 Hposχ I e B C hm₁ b
          rw [𝓖, 𝓖, not_imp_not, ← 𝓕, ← 𝓕] at ih₂𝓖
          simp only [*]
    | implication ψ χ ih₁ ih₂ =>
      cases ih₁ with | intro ih₁𝓕 ih₁𝓖 =>
      cases ih₂ with | intro ih₂𝓕 ih₂𝓖 =>
      constructor
      . intros Hpos I e
        rw [monotone] at *
        intros B C hm₁ m HB
        rw [𝓕, Pattern.interpret, ← 𝓕, ← 𝓕] at HB ⊢

        have Hpos := eq_false_of_ne_true Hpos
        simp only
          [Pattern.hasFreeNegativeOcc, Bool.decide_false_iff, not_or] at Hpos
        cases Hpos with
        | intro Hposψ Hposχ =>

        intros
        apply ih₂𝓕 Hposχ I e B C; assumption
        apply HB
        specialize ih₁𝓖 Hposψ I e B C hm₁ m
        rw [𝓖, 𝓖, not_imp_not, ← 𝓕, ← 𝓕] at ih₁𝓖
        simp only [*]
      . intros Hpos I e
        rw [monotone] at *
        intros B C hm₁ m HB
        rw [𝓖, Pattern.interpret, ← 𝓕, ← 𝓕] at HB ⊢

        have Hpos := eq_false_of_ne_true Hpos
        simp only
          [Pattern.hasFreePositiveOcc, Bool.decide_false_iff, not_or] at Hpos
        cases Hpos with
        | intro Hposψ Hposχ =>

        intro Hcontra
        apply HB
        intros
        specialize ih₂𝓖 Hposχ I e B C hm₁ m
        rw [𝓖, 𝓖, not_imp_not, ← 𝓕, ← 𝓕] at ih₂𝓖
        specialize ih₁𝓕 Hposψ I e B C hm₁ m
        apply ih₂𝓖; apply Hcontra; apply ih₁𝓕; assumption
    | existential x ψ ih =>
      cases ih with
      | intro ih₁𝓕 ih₁𝓖 =>
      constructor
      . intros Hpos I e
        rw [monotone] at *
        intros B C hm₁ m HB
        simp only [𝓕, Pattern.interpret, Valuation] at HB ⊢
        rw [Pattern.hasFreeNegativeOcc] at Hpos

        cases HB with
        | intro a Ha =>
        exists a
        specialize ih₁𝓕 Hpos I ⟨Function.update e.evalEvar x a, e.evalSvar⟩ B C hm₁ m
        apply ih₁𝓕
        apply Ha
      . intros Hpos I e
        rw [monotone] at *
        intros B C hm₁ m HB
        simp only [𝓖, Pattern.interpret, Valuation] at HB ⊢
        rw [Pattern.hasFreePositiveOcc] at Hpos

        intro ⟨a, Ha⟩
        apply HB
        exists a
        specialize
          ih₁𝓖 Hpos I ⟨Function.update e.evalEvar x a, e.evalSvar⟩ B C hm₁ m
        rw [𝓖, 𝓖, ← 𝓕, ← 𝓕, not_imp_not] at ih₁𝓖
        apply ih₁𝓖
        apply Ha
    | mu Z ψ ih =>
      apply @Classical.byCases (Z = X)
      . intros Heq
        simp only
          [ Heq, if_pos, monotone
          , Pattern.hasFreeNegativeOcc, Pattern.hasFreePositiveOcc
          ]
        constructor <;>
        ( intros _ I e B C _ m
          simp only [𝓕, 𝓖]
          rw
            [ only_free_var_relevant e.evalEvar e.evalEvar
              (Function.update e.evalSvar X B)
              (Function.update e.evalSvar X C)
            ]; exact id
          . intros; rfl
          . intro X' Hf
            simp only [Pattern.isFreeSvar] at Hf
            apply @Classical.byCases (X' = X)
            . intros; simp only [*, if_pos] at Hf;
            . intros; simp only [*, Function.update, dif_neg]
        )
      . intros Hneq
        cases ih with
        | intro ih𝓕 ih𝓖 =>
        constructor
        . intro Hpos I e
          have Hpos := eq_false_of_ne_true Hpos
          simp only [Pattern.hasFreeNegativeOcc] at Hpos
          rw [if_neg] at Hpos <;> (try (intro; apply Hneq; symm; assumption))
          simp only [monotone]
          intros B C hm₁ m
          simp only [𝓕, Pattern.interpret]
          intros HB D; specialize HB D
          rw [Function.update_comm] at *
            <;> (try (intro; apply Hneq; symm; assumption))        
          let e_zd : Valuation M := ⟨e.evalEvar, (Function.update e.evalSvar Z D)⟩
          have fe_eq : e.evalEvar = e_zd.evalEvar := rfl
          have
            fs_eq₁ :
              Function.update (Function.update e.evalSvar Z D) X B
                =
              Function.update e_zd.evalSvar X B
            := by simp only [Function.update]
          have
            fs_eq₂ :
              Function.update (Function.update e.evalSvar Z D) X C
                =
              Function.update e_zd.evalSvar X C
            := by simp only [Function.update]
          rw [fe_eq, fs_eq₁, ← 𝓕] at HB
          rw [fe_eq, fs_eq₂, ← 𝓕]
          intros HiC
          apply HB
          intros m H𝓕B
          apply HiC
          apply ih𝓕 <;> try assumption
          . apply ne_true_of_eq_false; assumption
        . intro Hpos I e
          simp only [monotone]
          intros B C hm₁ m
          simp only [𝓖, Pattern.interpret]
          rw [not_imp_not]
          intros HC D;
          specialize HC D
          rw [Function.update_comm] at *
            <;> (try (intro; apply Hneq; symm; assumption))    
          let e_zd : Valuation M := ⟨e.evalEvar, (Function.update e.evalSvar Z D)⟩
          have fe_eq : e.evalEvar = e_zd.evalEvar := rfl
          have
            fs_eq₁ :
              Function.update (Function.update e.evalSvar Z D) X B
                =
              Function.update e_zd.evalSvar X B
            := by simp only [Function.update]
          have
            fs_eq₂ :
              Function.update (Function.update e.evalSvar Z D) X C
                =
              Function.update e_zd.evalSvar X C
            := by simp only [Function.update]
          
          rw [fe_eq, fs_eq₂, ← 𝓕] at HC
          rw [fe_eq, fs_eq₁, ← 𝓕]
          intros HiB; apply HC; intros m H𝓕C; apply HiB; revert H𝓕C
          have Hneq : ¬ X = Z := by intro; apply Hneq; symm; assumption
          simp only [Pattern.hasFreePositiveOcc, Hneq, if_false] at Hpos
          specialize ih𝓖 Hpos I e_zd B C hm₁ m
          rw [𝓖, 𝓖, not_imp_not, ← 𝓕, ← 𝓕] at ih𝓖
          assumption

  set_option Pattern.pp_interpret_hiding false in 
  -- Similar to Proposition 3.75
  theorem subf_Xδ_eq_eval_Xeδ {X : SVar} {δ : Pattern 𝕊} :
    ∀ (m : M) (e : Valuation M),
    Pattern.substitutableForSvarIn X δ φ →  
    Pattern.interpret I e (Pattern.substSvar φ X δ) m
      =
      φ.interpret I
        ⟨ e.evalEvar, Function.update e.evalSvar X (Pattern.interpret I e δ) ⟩
        m
  := by
    apply @Classical.byCases (φ.isFreeSvar X)
    . intros Hfv
      induction φ with
      | evar ev =>
        intros
        rw [Pattern.isFreeSvar] at Hfv; contradiction
      | svar sv =>
        intros;
        simp only [Pattern.interpret, Function.update, Pattern.substSvar]
        apply @Classical.byCases (X = sv)
        . intro Heq;
          simp only [Heq, ite_true, eq_rec_constant, dite_eq_ite]
        . intro Hneq
          have Hneq' : ¬ sv = X := by intro; apply Hneq; symm; assumption
          simp only
            [ Hneq, Hneq', ite_false, eq_rec_constant, dite_eq_ite
            , Pattern.interpret
            ]
      | bottom => intros; simp only [Pattern.interpret]
      | symbol => intros; simp only [Pattern.interpret]
      | application ψ χ ih₁ ih₂ =>
        simp only [Pattern.interpret, Pattern.substitutableForSvarIn, Bool.decide_iff]
        intro m e ⟨Hsφ, Hsχ⟩
        simp only [eq_iff_iff]
        apply Iff.intro
        . intro ⟨a₁, a₂, h₁, h₂, h₃⟩
          exists a₁; exists a₂
          apply And.intro <;> try apply And.intro <;> try assumption
          . apply @Classical.byCases (ψ.isFreeSvar X)
            . intros
              rw [← ih₁] <;> try assumption
            . intro Hnot_fvψ
              rw [← case_not_fv ψ] <;> try assumption
          . apply @Classical.byCases (χ.isFreeSvar X)
            . intro Hfvχ
              rw [← ih₂] <;> try assumption
            . intro Hnot_fvχ
              rw [← case_not_fv χ] <;> try assumption
        . intro ⟨a₁, a₂, h₁, h₂, h₃⟩
          exists a₁; exists a₂
          apply And.intro <;> try apply And.intro <;> try assumption
          . apply @Classical.byCases (ψ.isFreeSvar X)
            . intros
              rw [ih₁] <;> try assumption
            . intros
              rw [case_not_fv ψ] <;> try assumption
          . apply @Classical.byCases (χ.isFreeSvar X)
            . intros
              rw [ih₂] <;> try assumption
            . intros
              rw [case_not_fv χ] <;> try assumption
      | implication ψ χ ih₁ ih₂ =>
        simp only [Pattern.interpret, Pattern.substitutableForSvarIn, Bool.decide_iff]
        intro m e ⟨Hsφ, Hsχ⟩
        apply @Classical.byCases (ψ.isFreeSvar X)
        . intros
          rw [ih₁] <;> try assumption
          apply @Classical.byCases (χ.isFreeSvar X)
          . intros
            rw [ih₂] <;> try assumption
          . intro
            rw [case_not_fv χ] <;> try assumption
        . intro
          rw [case_not_fv ψ] <;> try assumption
          apply @Classical.byCases (χ.isFreeSvar X)
          . intro
            rw [ih₂] <;> try assumption
          . intro
            rw [case_not_fv χ] <;> try assumption
      | existential z ψ ih =>
        simp only [Pattern.isFreeSvar] at Hfv; try assumption
        simp only
          [ Pattern.substitutableForSvarIn, Pattern.isFreeSvar, Hfv
          , Bool.ite_eq_true_distrib, Bool.decide_iff
          , if_true_right_eq_or, or_def_imp_neg
          ]
        intro m e H
        cases H True.intro with | intro H₁ H₂ =>
        simp only [Pattern.substEvar, Pattern.interpret, eq_iff_iff]
        have
          eqδ : Pattern.interpret I e δ = λ x => Pattern.interpret I e δ x := rfl
        apply Iff.intro
        . intro ⟨a, hₐ⟩
          exists a
          rw [ih] at hₐ <;> try assumption
          simp only [Valuation] at hₐ
          rw [eqδ]
          let e' : Valuation M := ⟨Function.update e.evalEvar z a, e.evalSvar ⟩ 
          have
            eqδ' : forall x, Pattern.interpret I e δ x = Pattern.interpret I e' δ x
              := by
            intro x
            rw
              [ only_free_var_relevant
                  e.evalEvar
                  e'.evalEvar
                  e.evalSvar
                  e'.evalSvar
              ]
            . intros x' f; simp only [Valuation, Function.update]
              apply @byCases (x' = z)
              . intros Heq; simp only [Heq] at f; contradiction
              . intros Hneq; simp only [Hneq, dif_neg];
            . intros X' _; simp only [Valuation]
          simpa only [eqδ']
        . intro ⟨a, hₐ⟩
          exists a
          rw [ih] <;> try assumption
          simp only [Valuation] at hₐ ⊢
          rw [eqδ] at hₐ
          let e' : Valuation M := ⟨Function.update e.evalEvar z a, e.evalSvar ⟩ 
          have
            eqδ' : forall x, Pattern.interpret I e δ x = Pattern.interpret I e' δ x
              := by
            intro x
            rw
              [ only_free_var_relevant
                  e.evalEvar
                  e'.evalEvar
                  e.evalSvar
                  e'.evalSvar
              ]
            . intros x' f; simp only [Valuation, Function.update]
              apply @byCases (x' = z)
                <;> (intros; simp only [*, dif_neg] at *)
            . intros; simp only [Valuation]
          simp only [eqδ'] at hₐ
          assumption
      | mu Z ψ ih =>
        intro m e Hs
        simp only [Pattern.interpret, eq_iff_iff, Pattern.substSvar]
        simp only [Pattern.substitutableForSvarIn, Hfv, if_true, Bool.decide_iff]
          at Hs
        cases Hs with
        | intro Hfvδ Hs =>
        simp only
          [Pattern.isFreeSvar, Bool.ite_eq_true_distrib, if_false_left_eq_and]
          at Hfv
        cases Hfv with
        | intro Hneq Hfv =>
        simp only [Hneq, if_false, Pattern.interpret]
        apply Iff.intro
        . intros H₁ B H₂
          let e_zb : Valuation M := ⟨e.evalEvar, Function.update e.evalSvar Z B ⟩
          apply H₁
          intros m' H
          rw [ih Hfv m' e_zb Hs] at H
          have
            eqδ : Pattern.interpret I e_zb δ = λ x => Pattern.interpret I e_zb δ x := rfl
          rw [eqδ] at H
          have
            eqδ' : forall x, Pattern.interpret I e_zb δ x = Pattern.interpret I e δ x
              := by
            intro x
            rw
              [ only_free_var_relevant
                  e.evalEvar
                  e_zb.evalEvar
                  e.evalSvar
                  e_zb.evalSvar
              ]
            . intros; simp only [Valuation]
            . intros X' f; simp only [Valuation, Function.update]
              apply @byCases (X' = Z)
                <;> (intros; simp only [*, dif_neg] at *)
          simp only [eqδ'] at H
          apply H₂
          rw [Function.update_comm Hneq]
          exact H
        . intros H₂ B H₁
          let e_zb : Valuation M := ⟨e.evalEvar, Function.update e.evalSvar Z B ⟩
          apply H₂
          intros m' H
          apply H₁
          rw [ih Hfv m' e_zb Hs]
          have
            eqδ : Pattern.interpret I e_zb δ = λ x => Pattern.interpret I e_zb δ x := rfl
          rw [eqδ]
          have
            eqδ' : forall x, Pattern.interpret I e_zb δ x = Pattern.interpret I e δ x
              := by
            intro x
            rw
              [ only_free_var_relevant
                  e.evalEvar
                  e_zb.evalEvar
                  e.evalSvar
                  e_zb.evalSvar
              ]
            . intros; simp only [Valuation]
            . intros X' f; simp only [Valuation, Function.update]
              apply @byCases (X' = Z)
                <;> (intros; simp only [*, dif_neg] at *)
          simp only [eqδ']
          rw [Function.update_comm]
            <;> try (intro contra; simp only [Eq.symm contra] at Hneq)
          exact H
    . intros Hnot_fv
      apply case_not_fv φ Hnot_fv
  where
    case_not_fv {X : SVar} {δ : Pattern 𝕊}
      φ (Hnot_fv : ¬Pattern.isFreeSvar φ X = true) m e :
    Pattern.substitutableForSvarIn X δ φ →  
    Pattern.interpret I e (Pattern.substSvar φ X δ) m
      =
      φ.interpret I
        ⟨ e.evalEvar, Function.update e.evalSvar X (Pattern.interpret I e δ) ⟩
        m
    := by
      intros
      rw [Pattern.subst_not_free_svar _ _ _ Hnot_fv, only_free_var_relevant]
      . intros
        simp only [Function.update]
      . intros X' H'
        simp only [Function.update]
        apply @Classical.byCases (X' = X)
        . intros eq
          simp only [eq] at *; contradiction
        . intros neq
          simp only [neq, dif_neg]

  set_option Pattern.pp_interpret_hiding false in 
  theorem prefixpoint_sound {X : SVar}
    (φ_pos_X: ¬Pattern.hasFreeNegativeOcc φ X = true)
    (sfi : Pattern.substitutableForSvarIn X (μ X φ) φ = true) :
    Γ ⊧ φ[X ⇐ˢ μ X φ] ⇒ μ X φ
  := by
    intros
    intro M _ I _ e m
    simp only [Pattern.interpret]
    intro 𝓕_μ𝓕
    rw [subf_Xδ_eq_eval_Xeδ] at 𝓕_μ𝓕 <;> try assumption
    
    have monotone𝓕 : monotone (𝓕 I e φ X) := by
      apply (𝓕𝓖_monotone φ X).1; assumption
    
    -- Case "⊆" of Claim 1 from the Knaster-Tarski Theorem
    intro B
    rw [← 𝓕]
    intro HB
    rw [← 𝓕] at 𝓕_μ𝓕
    apply HB
    apply monotone𝓕 (Pattern.interpret I e (μ X φ))
    simp only [Pattern.interpret]
    intro m Hμ
    apply Hμ
    intros m' Hm'
    rw [← 𝓕] at Hm'
    apply HB; apply Hm'; exact 𝓕_μ𝓕

  set_option Pattern.pp_interpret_hiding false in
  theorem substitution_sound {X : SVar} (sfi : ψ.substitutableForSvarIn X φ) :
    Γ ⊧ φ → Γ ⊧ Pattern.substSvar φ X ψ
  := by
    intros h M inst I hΓ e m
    let
      e' : Valuation M :=
        ⟨e.evalEvar, Function.update e.evalSvar X (Pattern.interpret I e ψ)⟩
    specialize h hΓ e' m
    rw [subf_Xδ_eq_eval_Xeδ] <;> assumption

  set_option Pattern.pp_interpret_hiding false in 
  -- Similar to Proposition 3.106
  theorem subf_impl_mu_implₗ {X : SVar}
    (sfi : Pattern.substitutableForSvarIn X ψ φ) : ∀ I (e : Valuation M),
      (Valuation.satifies I e (φ[X ⇐ˢ ψ] ⇒ ψ) → Valuation.satifies I e (μ X φ ⇒ ψ))
  := by
    simp only [Valuation.satifies]
    intros I e H
    intro m
    simp only [interpret_impl]
    intro Hμ
    simp only [Pattern.interpret] at Hμ H
    apply Hμ
    intro m₀
    specialize H m₀
    rw [subf_Xδ_eq_eval_Xeδ] at H
    apply H; assumption

  set_option Pattern.pp_interpret_hiding false in 
  -- Similar to Proposition 3.108
  theorem subf_impl_mu_impl {X : SVar}
    (sfi : Pattern.substitutableForSvarIn X ψ φ) : ∀ I : Interpretation 𝕊 M,
      (I.satisfies (φ[X ⇐ˢ ψ] ⇒ ψ) → I.satisfies (μ X φ ⇒ ψ))
  := by
    simp only [Interpretation.satisfies]
    intros I H e
    apply subf_impl_mu_implₗ; assumption
    apply H e

  set_option Pattern.pp_interpret_hiding false in 
  -- Similar to Proposition 3.109
  theorem knasterTarski_sound {X : SVar}
    (sfi : Pattern.substitutableForSvarIn X ψ φ) :
      (ps : Γ ⊧ φ[X ⇐ˢ ψ] ⇒ ψ) → (Γ ⊧ μ X φ ⇒ ψ)
  := by
    simp only [entails]
    intros H m _ I a
    apply subf_impl_mu_impl; assumption
    apply H; assumption
  
  -- TODO (Andrei B): Global?
  theorem Proof.soundness {φ : Pattern 𝕊} : Γ ⊢ φ → Γ ⊧ φ 
  | premise _ prf => premise_sound prf 
  | tautology taut => tautology_sound taut
  | existQuan sfi => existQuan_sound sfi
  | propagationBottomLeft => propagationBottomLeft_sound 
  | propagationBottomRight => propagationBottomRight_sound 
  | propagationDisjLeft => propagationDisjLeft_sound 
  | propagationDisjRight => propagationDisjRight_sound 
  | propagationExistLeft nfv => propagationExistLeft_sound nfv 
  | propagationExistRight nfv => propagationExistRight_sound nfv
  | @prefixpoint _ _ ψ X wf sfi => by
    cases wf with
    | mu _ φ_pos_X =>
    apply prefixpoint_sound φ_pos_X sfi
  | existence => existence_sound 
  | singleton => singleton_sound 

  | modusPonens prf₁ prf₂ => modus_ponens_sound prf₁.soundness prf₂.soundness 
  | existGen Hnfv prf => existGen_sound Hnfv prf.soundness 
  | framingLeft prf => framing_left_sound prf.soundness    
  | framingRight prf => framing_right_sound prf.soundness 
  | substitution sfi prf => substitution_sound sfi prf.soundness 
  | knasterTarski sfi prf => knasterTarski_sound sfi prf.soundness 

end 
