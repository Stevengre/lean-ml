## Lean formalization of Applicative Matching Logic 

The technical report describes [this release](https://gitlab.com/ilds/aml-lean/MatchingLogic/-/tags/stage-1)

### Team
[Andrei Burdușa](https://gitlab.com/andreiburdusa) \
[Horațiu Cheval](https://cs.unibuc.ro/~hcheval/)  

#### Former members
[Bogdan Macovei](https://cs.unibuc.ro/~bmacovei/)
